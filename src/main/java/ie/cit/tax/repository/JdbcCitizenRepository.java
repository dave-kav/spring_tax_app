package ie.cit.tax.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ie.cit.tax.domain.Citizen;
import ie.cit.tax.rowmapper.CitizenRowMapper;

@Repository
public class JdbcCitizenRepository implements CitizenDAO {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcCitizenRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public Citizen get(int id) {
		String sql = "SELECT * FROM citizens WHERE id = ?";
		Citizen citizen = jdbcTemplate.queryForObject(sql, new Object[] { id },
				new CitizenRowMapper());
		return citizen;
	}

	@Override
	public void save(Citizen citizen) {
		if (citizen.getId() != 0) {
			update(citizen);
		} else {
			add(citizen);
		}
	}

	private void add(Citizen citizen) {
		String sql = "INSERT INTO Citizens (first_Name, second_name, salary) VALUES (?, ?, ?)";
		jdbcTemplate.update(sql, new Object[] { citizen.getFirstName(),
					citizen.getSecondName(), citizen.getSalary()} );
	}

	private void update(Citizen citizen) {
		String sql = "UPDATE Citizens SET first_name = ?, second_name = ? , salary = ? WHERE id = ?";
		jdbcTemplate.update(sql, new Object[] { citizen.getFirstName(),
			citizen.getSecondName(), citizen.getSalary(), citizen.getId()} );
	}
	
	@Override
	public List<Citizen> findAll() {
		String sql = "SELECT * FROM citizens";
		return jdbcTemplate.query(sql, new CitizenRowMapper());
	}
	
}
