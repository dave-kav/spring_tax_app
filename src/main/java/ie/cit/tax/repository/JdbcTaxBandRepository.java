package ie.cit.tax.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ie.cit.tax.domain.TaxBand;
import ie.cit.tax.rowmapper.TaxBandRowMapper;

@Repository
public class JdbcTaxBandRepository implements TaxBandDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcTaxBandRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public TaxBand get(String name) {
		String sql = "SELECT * FROM taxbands WHERE name = ?";
		TaxBand taxBand = jdbcTemplate.queryForObject(sql, new Object[] {name}, 
				new TaxBandRowMapper());
		return taxBand;
	}

	@Override
	public void save(TaxBand taxBand) {
		if (taxBand.getName() != null) {
			update(taxBand);
		}
	}

	private void update(TaxBand taxBand) {
		String sql = "UPDATE taxbands SET lower_limit = ? , upper_limit = ?, rate = ? WHERE name = ?";
		jdbcTemplate.update(sql, new Object[] { taxBand.getLowerLimit(), taxBand.getUpperLimit(), 
				taxBand.getRate(), taxBand.getName()});
	}

	@Override
	public List<TaxBand> findAll() {
		String sql = "SELECT * FROM taxbands ORDER BY name";
		return jdbcTemplate.query(sql, new TaxBandRowMapper());
	}

}
