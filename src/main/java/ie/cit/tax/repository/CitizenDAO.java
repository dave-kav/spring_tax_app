package ie.cit.tax.repository;

import java.util.List;

import ie.cit.tax.domain.Citizen;

public interface CitizenDAO {

	Citizen get(int id);
	
	void save(Citizen citizen);

	List<Citizen> findAll();
}
