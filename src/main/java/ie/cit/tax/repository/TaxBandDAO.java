package ie.cit.tax.repository;

import java.util.List;

import ie.cit.tax.domain.TaxBand;

public interface TaxBandDAO {

	TaxBand get(String name);
	
	void save(TaxBand taxBand);
	
	List<TaxBand> findAll();
}
