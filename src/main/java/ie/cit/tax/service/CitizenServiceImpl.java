package ie.cit.tax.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.cit.tax.domain.Citizen;
import ie.cit.tax.repository.CitizenDAO;

@Service
public class CitizenServiceImpl implements CitizenService {

	CitizenDAO citizenRepository;
	
	@Autowired
	public CitizenServiceImpl(CitizenDAO citizenRepository) {
		this.citizenRepository = citizenRepository;
	}
	
	@Override
	public Citizen get(int id) {
		return citizenRepository.get(id);
	}
	
	@Override
	public void save(Citizen citizen) {
		citizenRepository.save(citizen);
	}

	@Override
	public List<Citizen> findAll() {
		return citizenRepository.findAll();
	}
}
