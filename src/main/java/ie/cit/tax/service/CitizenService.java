package ie.cit.tax.service;

import java.util.List;

import ie.cit.tax.domain.Citizen;

public interface CitizenService {

	Citizen get(int id);
	
	void save(Citizen citizen);

	List<Citizen> findAll();

}
