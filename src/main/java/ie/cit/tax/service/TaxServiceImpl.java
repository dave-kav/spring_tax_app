package ie.cit.tax.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.cit.tax.domain.Citizen;
import ie.cit.tax.domain.TaxBand;
import ie.cit.tax.repository.TaxBandDAO;

@Service
public class TaxServiceImpl implements TaxService {
	
	TaxBandDAO taxbandRepository;
	
	@Autowired
	public TaxServiceImpl(TaxBandDAO taxbandRepository) {
		this.taxbandRepository = taxbandRepository;
	}

	@Override
	public TaxBand get(String name) {
		return taxbandRepository.get(name);
	}

	@Override
	public void save(TaxBand taxBand) {
		taxbandRepository.save(taxBand);
	}

	@Override
	public List<TaxBand> findAll() {
		return taxbandRepository.findAll();
	}
	
	@Override
	public double calculateTax(Citizen citizen) {		
		double tax = 0;
		int taxableSalary = citizen.getSalary(); 
		
		//solidarity tax
		tax += (taxableSalary/100)*taxbandRepository.get("D").getRate();
		
		//taxband a - exempt
		taxableSalary -= taxbandRepository.get("A").getUpperLimit();
		
		//taxband b
		if (taxableSalary > 0) {
			int band = taxbandRepository.get("B").getUpperLimit() - taxbandRepository.get("A").getUpperLimit();
			if (taxableSalary >= band) {
				taxableSalary -= band;
				tax += (band/100)*taxbandRepository.get("B").getRate();
			}
			else {
				tax += (taxableSalary/100)*taxbandRepository.get("B").getRate();
				taxableSalary = 0;
			}
		}
		//taxband c
		if (taxableSalary > 0) {
			tax += (taxableSalary/100)*taxbandRepository.get("C").getRate();
		}
		return tax;
	}
}
