package ie.cit.tax.service;

import java.util.List;

import ie.cit.tax.domain.Citizen;
import ie.cit.tax.domain.TaxBand;

public interface TaxService {

	TaxBand get(String name);

	void save(TaxBand taxBand);

	List<TaxBand> findAll();
	
	double calculateTax(Citizen citizen);

}
