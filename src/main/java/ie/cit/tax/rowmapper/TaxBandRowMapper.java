package ie.cit.tax.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ie.cit.tax.domain.TaxBand;

public class TaxBandRowMapper implements RowMapper<TaxBand> {

	@Override
	public TaxBand mapRow(ResultSet rs, int rowNum) throws SQLException {
		TaxBand taxBand = new TaxBand();
		
		taxBand.setName(rs.getString("name"));
		taxBand.setLowerLimit(rs.getInt("lower_limit"));
		taxBand.setUpperLimit(rs.getInt("upper_limit"));
		taxBand.setRate(rs.getInt("rate"));
		
		return taxBand;
	}
}
