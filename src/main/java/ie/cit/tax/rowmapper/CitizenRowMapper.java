package ie.cit.tax.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ie.cit.tax.domain.Citizen;

public class CitizenRowMapper implements RowMapper<Citizen> {

	@Override
	public Citizen mapRow(ResultSet rs, int rowNum) throws SQLException {
		Citizen citizen = new Citizen();
		
		citizen.setId(rs.getInt("id"));
		citizen.setFirstName(rs.getString("first_name"));
		citizen.setSecondName(rs.getString("second_name"));
		citizen.setSalary(rs.getInt("salary"));

		return citizen;
	}
}
