package ie.cit.tax;

import java.util.ArrayList;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import ie.cit.tax.domain.Citizen;
import ie.cit.tax.domain.TaxBand;
import ie.cit.tax.service.CitizenService;
import ie.cit.tax.service.TaxService;

@SpringBootApplication
public class Assignment1Application implements CommandLineRunner {

	@Autowired JdbcTemplate jdbcTemplate;
	@Autowired TaxService taxService;
	@Autowired CitizenService citizenService;
	
	static Scanner kbd = new Scanner(System.in);

	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("==============================");
		System.out.println("====Tax Calculation System====");
		System.out.println("==============================");

		int menuChoice = 0;
		while (menuChoice != 4) {
			System.out.println("============Options===========");
			System.out.println("1) List all citizen's details");
			System.out.println("2) Enter new citizen details");
			System.out.println("3) Change tax bands and rates");
			System.out.print("4) Quit: ");
			menuChoice = kbd.nextInt();

			switch(menuChoice) {
			case 1:	listAll(); break;

			case 2:	addCitizen(); break;

			case 3:	updateTaxBands(); break;

			case 4: System.out.println("Goodbye!"); System.exit(0);

			default: System.out.println("INVALID OPTION: Please choose from 1 - 4\n"); break;
			}			
		}
	}

	public void listAll() {
		ArrayList<Citizen> citizens = (ArrayList<Citizen>) citizenService.findAll();
		String out = 
				"\n==============================\n" + 
				"====Current Citizen Details===\n" +
				"==============================\n";
		for (Citizen c: citizens) {
			out += c.toString() + "\t";
			out += "Tax:" +  taxService.calculateTax(c) + "\t";
			out += "Nett Income: " + (c.getSalary() - taxService.calculateTax(c)) + "\n";
		}
		System.out.println(out + "\n");

		kbd.nextLine();
	}

	public void addCitizen() {
		String out = 
				"\n==============================\n" + 
				"====Add new Citizen Details===\n" +
				"==============================\n";
		System.out.println(out);
		kbd.nextLine();

		Citizen newCitizen = new Citizen();
		System.out.print("Enter first name: ");
		newCitizen.setFirstName(kbd.nextLine());
		System.out.print("Enter second name: ");
		newCitizen.setSecondName(kbd.nextLine());
		System.out.print("Enter salary: ");
		newCitizen.setSalary(kbd.nextInt());
		kbd.nextLine();
		
		citizenService.save(newCitizen);
		System.out.print("\n");
	}

	public void updateTaxBands() {
		ArrayList<TaxBand> taxBands = (ArrayList<TaxBand>) taxService.findAll();

		String out = 
				"==============================\n" + 
				"====Current TaxBand Details===\n" +
				"==============================\n";
		for (TaxBand t: taxBands) {
			out += t.toString() + "\n";
		}
		System.out.println(out);

		kbd.nextLine();

		System.out.print("Enter the letter of the tax band you wish to edit,\nEnter any other key to return: ");
		char taxLetter = kbd.nextLine().toUpperCase().charAt(0);
		TaxBand taxBand = null;
		
		try {
			taxBand = taxService.get(String.valueOf(taxLetter));
		} catch (EmptyResultDataAccessException e) {
			return;
		}

		System.out.print("Enter lower limit: ");
		int lowerLimit = kbd.nextInt();
		System.out.print("Enter the upper limit: ");
		int upperLimit = kbd.nextInt();
		System.out.print("Enter the rate: ");
		int rate = kbd.nextInt();

		taxBand.setLowerLimit(lowerLimit);
		taxBand.setUpperLimit(upperLimit);
		taxBand.setRate(rate);

		taxService.save(taxBand);
		System.out.println("\nTax Band updated!!\n");
	}

	public static void main(String[] args) {
		SpringApplication.run(Assignment1Application.class, args);
	}
}
