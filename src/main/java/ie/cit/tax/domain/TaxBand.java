package ie.cit.tax.domain;

public class TaxBand {
	private String name;	
	private int lowerLimit;
	private int upperLimit;
	private int rate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getLowerLimit() {
		return lowerLimit;
	}
	
	public void setLowerLimit(int lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	
	public int getUpperLimit() {
		return upperLimit;
	}
	
	public void setUpperLimit(int upperLimit) {
		this.upperLimit = upperLimit;
	}
	
	public int getRate() {
		return rate;
	}
	
	public void setRate(int rate) {
		this.rate = rate;
	}
	
	@Override
	public String toString() {
		if (name.equals("C"))
			return "Taxband " + name + ": " + lowerLimit + "+ - " + rate + "%";
		else if (name.equals("D"))
			return "Taxband " + name + ": Solidarity Tax - " + rate + "%";
		else 
			return "Taxband " + name + ": " + lowerLimit + " to " + upperLimit + " - " + rate + "%";
	}
}
