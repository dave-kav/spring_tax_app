INSERT INTO taxbands (name, lower_limit, upper_limit, rate) VALUES ('A', 0, 10000, 0);
INSERT INTO taxbands (name, lower_limit, upper_limit, rate) VALUES ('B', 10001, 25000, 20);
INSERT INTO taxbands (name, lower_limit, rate) VALUES ('C', 25001, 40);
INSERT INTO taxbands (name, rate) VALUES ('D', 3);

INSERT INTO citizens (first_name, second_name, salary) VALUES ('Bertie', 'Ahern', 19500);
INSERT INTO citizens (first_name, second_name, salary) VALUES ('Michael', 'Martin', 36000);
INSERT INTO citizens (first_name, second_name, salary) VALUES ('Ryan', 'Tubridy', 500000);
INSERT INTO citizens (first_name, second_name, salary) VALUES ('Pat', 'Kenny', 750000);
INSERT INTO citizens (first_name, second_name, salary) VALUES ('Ray', 'Darcy', 120000);